/*
* The MIT License
*
* CSVtoXML
*
* Copyright 2015 Ibbtek <http://ibbtek.altervista.org/>.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

package com.ibbtek.utilities;

import static com.ibbtek.csvtoxml.MainGui.csv;
import static com.ibbtek.csvtoxml.MainGui.jProgressBar1;
import static com.ibbtek.csvtoxml.MainGui.nb;
import static com.ibbtek.csvtoxml.MainGui.rowName;
import static com.ibbtek.csvtoxml.MainGui.delimiter;
import static com.ibbtek.csvtoxml.MainGui.xml;
import static com.ibbtek.utilities.LogToFile.log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * BuildXml class
 * @author Ibbtek <ibbtek@gmail.com>
 */
public class BuildXml implements Runnable{
    
    private String[] fields;
    @Override
    public void run(){
        BufferedReader in = null;
        boolean res=true;
        try {
            /*
            Open the CSV file in reader mode and eventualy clear the XML file
            */
            in = new BufferedReader(new FileReader(csv));
            res=clearFile(xml);
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("root");
            doc.appendChild(rootElement);
            for(int i=0; i<nb;i++){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    break;
                }
                /*
                Increment the progress bar and get the first line of the CSV
                file
                */
                jProgressBar1.setValue(i);
                String line = in.readLine();
                if(i==0){
                    fields = line.split(delimiter);
                }else{
                    Element rowElement = doc.createElement(rowName);
                    rootElement.appendChild(rowElement);
                    /*
                    Get every field in a String Array then put it
                    between the proper tag containing the proper field
                    name
                    */
                    String[] word = line.split(delimiter);
                    for(int k=0;k<word.length;k++){
                        Element element = doc.createElement(fields[k]);
                        element.appendChild(doc.createTextNode(word[k]));
                        rowElement.appendChild(element);
                    }
                }
                // write the content into xml file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(xml);
                
                // Output to console for testing
                //StreamResult result = new StreamResult(System.out);
                
                transformer.transform(source, result);
            }
            
        } catch (ParserConfigurationException | TransformerException ex) {
            log(ex,"config","Can't convert the CSV file to XML file.");
            Thread.currentThread().interrupt();
            res=false;
        } catch (FileNotFoundException ex) {
            log(ex,"config","Can't open the CSV file.");
            Thread.currentThread().interrupt();
            res=false;
        } catch (IOException ex) {
            log(ex,"config","Can't read the CSV file.");
            Thread.currentThread().interrupt();
            res=false;
        }finally{
            try {
                /*
                Close the CSV file anyway
                */
                in.close();
            } catch (IOException ex) {
                log(ex,"config","Can't close CSV file.");
                Thread.currentThread().interrupt();
                res=false;
            }
        }
        
        if(res){
            JOptionPane.showMessageDialog(null,"Convertion successful",
                    "Info", JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null,"Error, check the log file",
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    /**
     * clearFile Method
     * That clear completly the file
     * @param file
     * @return boolean to say if something wrong happend
     */
    private boolean clearFile(File file){
        boolean result=true;
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file);
            writer.print("");
        } catch (FileNotFoundException ex) {
            log(ex,"config","Can't clear XML file.");
            result=false;
        }finally{
            writer.close();
        }
        return result;
    }
}
